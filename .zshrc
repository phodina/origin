# Enable autocompletion (use cache for faster startup)
autoload -Uz compinit
typeset -i updated_at=$(date +'%j' -r ~/.zcompdump 2>/dev/null || stat -f '%Sm' -t '%j' ~/.zcompdump 2>/dev/null)
if [ $(date +'%j') != $updated_at ]; then
    compinit -i
  else
    compinit -C -i
fi

[ -f "~/.aliases" ] && source ~/.aliases

# Provides a menu list to highlight and select completion results
zmodload -i zsh/complist

setopt auto_list # automatically list choices on ambiguous completion
setopt auto_menu # automatically use menu completion
setopt always_to_end # move cursor to end if word had one match

zstyle ':completion:*' menu select # select completions with arrow keys
zstyle ':completion:*' group-name '' # group results by category
zstyle ':completion:::::' completer _expand _complete _ignored _approximate # enable approximate matches for completion

# Save history
HISTFILE=${HOME}/.zsh_history
HISTSIZE=100000
SAVEHIST=${HISTSIZE}

setopt hist_ignore_all_dups # remove older duplicate entries from history
setopt hist_reduce_blanks # remove superfluous blanks from history items
setopt inc_append_history # save history entries as soon as they are entered
setopt share_history # share history between different instances of the shell

setopt auto_cd # cd by typing directory name if it's not a command

# Fix delete key    
bindkey '^[[3~' delete-char
bindkey '^[3;5~' delete-char

antibody bundle < ~/.zsh_plugins.txt > ~/.zsh_plugins.sh
source ~/.zsh_plugins.sh

# SSH and GPG settings
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
gpgconf --launch gpg-agent

export GPG_TTY=$(tty)

# Source aliases
[ -d "${HOME}/.origin" ] && alias origin='$(which git) --git-dir=${HOME}/.origin/ --work-tree=${HOME}/'
[ -f "${HOME}/.aliases" ] && source ${HOME}/.aliases

export STARSHIP_CONFIG=~/.config/starship.toml
eval "$(starship init zsh)"

