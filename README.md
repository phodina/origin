# Home
Make yourself at home **TLDR;**

Use `git` to manage `dotfiles`. However, don't use symlinks but *bare* repository and *work-tree*.

## Initialization
```shell
mkdir ~/.origin
git init --bare ~/.origin
alias origin='$(which git) --git-dir=~/.origin/ --work-tree=~/'
origin config --local status.showUntrackedFiles no
origin remote add origin git@gitlab.com:phodina/origin.git
origin config --local status.showUntrackedFiles no
```

## Commit file
```shell
cd ${HOME}
origin add README.md
origin commit 
origin push
```
## Config

home config --local status.showUntrackedFiles no
## Setup new machine
```shell
git clone --separate-git-dir=~/.origin git@gitlab.com:phodina/home.git /tmp/origin
rsync --recursive --verbose --exclude '.git' /tmp/origin ~/
rm -r /tmp/origin
```

## Credits
[Ask HN: What do you use to manage dotfiles?](https://news.ycombinator.com/item?id=11070797)
