export MOZ_ENABLE_WAYLAND=1
export HOST=${HOST}
export WALLPAPERS=${HOME}/Pictures/Wallpapers
export SCREENSHOTS=${HOME}/Pictures/screenshots

[ ! -e "${WALLPAPERS}" ] && mkdir -p "${WALLPAPERS}"
[ ! -e "${SCREENSHOTS}" ] && mkdir -p "${SCREENSHOTS}"
